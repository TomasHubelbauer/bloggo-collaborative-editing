# Collaborative Editing

> Here I hope to collect links to technologies and applications enabling collaborative editing.

## Tandem

State representation: CRDT

- [GitHub repository](https://github.com/typeintandem/tandem)
- [Web site](http://typeintandem.com/)
